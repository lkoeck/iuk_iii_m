"use strict";

this.addEventListener('fetch', function (event) {
  // TODO
    if(event.request.headers.get('save-data')){
        if(event.request.url.includes('fonts.googleapis.com')){
            event.respondWith(new Response('',{status: 417, statusText: 'Ignore fonts to save data.'}));
        }
    }
});