self.addEventListener('install', function(event) {
    self.skipWaiting();
    console.log('Installed', event);
});

self.addEventListener('activate', function(event) {
    console.log('Activated', event);
});

self.addEventListener('fetch', function(event){
    if(/\.jpg$/.test(event.request.url)){
        event.respondWith(fetch('./images/unicorn.jpg'));
    }
});